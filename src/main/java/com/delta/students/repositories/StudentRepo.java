package com.delta.students.repositories;
import com.delta.students.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
public interface StudentRepo extends JpaRepository<Student, Long> {
}