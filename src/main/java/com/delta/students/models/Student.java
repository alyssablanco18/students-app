// import java.lang.invoke.InjectedProfile;

// import javax.annotation.processing.Generated;

// import jdk.nashorn.internal.objects.annotations.Getter;
// import jdk.nashorn.internal.objects.annotations.Setter;

// @Entity
// @Table(name = "students")
// @Getter
// @Setter
// public class Student {

//     @Id
//     @GeneratedValue(strategy = GenerationType.IDENTITY)
//     private Long id;

//     private String firstName;

//     private String lastName;

//     private String studentID;

//     private String email;

//     private Number gradeLevel;

//     private Number dateOfBirth;

//     private Number emergencyContactNumber;


// }


package com.delta.students.models;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
@Entity
@Table(name = "students")
@Getter
@Setter
public class Student {
    // variables / columns
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String firstName;

    private String lastName;

    private String studentID;

    private String email;

    private int gradeLevel;

    private String dateOfBirth;
    
    private String emergencyContactNumber;
}