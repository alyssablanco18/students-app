// import com.delta.students.models.Student;

// public interface StudentService {

//     List<Student> findAll();

//     // method to save an object of our model - "Student"
//     Student save(Student student);

//     // method to find a object by the id
//     Student findById(Long id);

//     // method to delete a object from our database
//     void delete(Long id);
// }

package com.delta.students.services;
import java.util.List;
import com.delta.students.models.Student;
public interface StudentService {
    List<Student> findAll();
    // method to save
    Student save(Student student);

    // method to find an object by id
    Student findById(Long id);
    
    //method to delete an object from the database
    void delete(Long id);
}