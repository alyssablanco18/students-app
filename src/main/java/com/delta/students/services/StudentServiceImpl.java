// import java.util.List;

// import com.delta.students.repositories.StudentRepo;

// @Service
// public class StudentServiceImpl implements StudentService {

//     // bring in our repo
//     @AutoWired
//     StudentRepo studentRepo;

//     @Override
//     public List<Student> findAll() {

//         return studentRepo.findAll();
//     }

//     @Override
//     public Student save(Student student) {
//         studentRepo.save(student);
//         return student;
//     }

//     @Override
//     public Student findById(Long id) {
//         if(StudentRepo.findById(id).isPresent()) {
//             return studentRepo.findById(id).get();
//         }
//     }


//     @Override
//     public void delete(Long id) {
//         Student studentToDelete = findById(id);
//         studentRepo.delete(studentToDelete);
//     }
// }


package com.delta.students.services;
import java.io.Serializable;
import java.util.List;
import com.delta.students.models.Student;
import com.delta.students.repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class StudentServiceImpl implements StudentService, Serializable {
    // bring in our repo
    @Autowired
    StudentRepo studentRepo;

    @Override
    public List<Student> findAll() {
        return studentRepo.findAll();
    }
    @Override
    public Student save(Student student) {
        studentRepo.save(student);
        return student;
    }
    @Override
    public Student findById(Long id) {
        if(studentRepo.findById(id).isPresent()) {
            return studentRepo.findById(id).get();
        }
        return null;
    }
    @Override
    public void delete(Long id) {
        Student studentToDelete = findById(id);
        studentRepo.delete(studentToDelete);
    }
}