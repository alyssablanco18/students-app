// import java.util.List;

// @CrossOrigin("*")
// @RestController
// @RequestMapping("/api/v2")
// public class StudentController {

//     @AutoWired
//     StudentService sService;

//     // get method to get the data from our database -> 
//     // table named 'students'
//     @GetMapping("/students")
//     public ResponseEntity<List<Student>> get() {
//         List<Student> students = sService.findAll();

//         return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
//     }

//     // post method to post new data to our database
//     @PostMapping("/students")
//     public ResponseEntity<Student> save(@RequestBody Student student) {
//         Student newStudent = sService.save(student);

//         return new ResponseEntity<Student>(newStudent, HttpStatus.OK);
//     }

//     // get method to GET data of an individual post
//     // object based of the id property
//     @GetMapping("/students/{id}")
//     public ResponseEntity<Student> getStudent(@PathVariable("id") Long id) {
//         Student viewStudent = sService.findById(id);

//         return new ResponseEntity<Student>(viewStudent, HttpStatus.OK);
//     }

//     // delete method to delete the data of an individual post object
//     // based off the objects id
//     @DeleteMapping("/students/{id}")
//     public ResponseEntity<String> delete(@PathVariable("id") Long id) {
//         sService.delete(id);

//         return new ResponseEntity<String>("Student is deleted successfully", HttpStatus.OK);
//     }
// }



package com.delta.students.controllers;
import java.util.List;
import com.delta.students.models.Student;
import com.delta.students.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin("*")
@RestController
@RequestMapping("/api/s1")
public class StudentController {
    // bring in our student service
    @Autowired
    StudentService sService;
    // create a get method to GET the data from our database ->
    // table named "students"
    @GetMapping("/students")
    public ResponseEntity<List<Student>> get() {
        List<Student> students = sService.findAll();
        return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
    }
    // method to POST new data to our database
    @PostMapping("/students")
    public ResponseEntity<Student> save(@RequestBody Student student) {
        Student newStudent = sService.save(student);
        return new ResponseEntity<Student>(newStudent, HttpStatus.OK);
    }
    // get method to GET the data of an individual Student object
    // based off of the id properties
    @GetMapping("/students/{id}")
    public ResponseEntity<Student> getStudent(@PathVariable("id") Long id) {
        Student viewStudent = sService.findById(id);
        return new ResponseEntity<Student>(viewStudent, HttpStatus.OK);
    }
    //delete method to Delete the data of an individual Student object
    // based off of the object's id
    @DeleteMapping("/students/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        sService.delete(id);
        return new ResponseEntity<String>("Student has been successfully deleted!", HttpStatus.OK);
    }
}